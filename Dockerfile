ARG REPO_ARCH

FROM linuxserver/nextcloud:$REPO_ARCH-NEXTCLOUD_VERSION

ARG TARGET_TRIPLE

ADD https://github.com/nextcloud/notify_push/releases/download/v0.6.12/notify_push-$TARGET_TRIPLE /usr/local/bin/notify_push
RUN chmod +x /usr/local/bin/notify_push

COPY root/ /
